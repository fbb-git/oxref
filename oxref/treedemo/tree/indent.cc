#include "tree.ih"

void Tree::indent(size_t level) const
{
    for (size_t idx = 0; idx != level; ++idx)
    {
        auto &data  = d_info[idx];
        cout << (data.current != data.callSize ? "| " : "  ");
    }

    cout << "+-";
}
