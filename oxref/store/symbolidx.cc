#include "store.ih"

size_t Store::symbolIdx(string const &symbol) const
{
    return find_if(d_xrefVector.begin(), d_xrefVector.end(), 
                [&](XrefData const &data)
                {
                    return findSymbol(data, symbol);
                }
            ) - d_xrefVector.begin();
}
