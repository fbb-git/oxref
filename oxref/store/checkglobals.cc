#include "store.ih"

void Store::checkGlobals(IdxIter &begin, IdxIter &end)
{
    auto iter = find_if(begin, end,             // find a GLOBALS entry
        [&](size_t idx)
        {
            return d_xrefVector[idx].symbol().find("GLOBALS") != string::npos;
        }
    );

    if (iter == end)                            // no such entry
    {
        d_globals.first = d_xrefVector.size();
        return;
    }

    d_globals.first = iter - begin;             // the entry having GLOBALS

                                                // find a function name
    istringstream names{ d_xrefVector[d_globals.first].symbol() };
    string name;
   
    while (names >> name)                       // find a name != GLOBALS
    {
        if (name != "GLOBALS")
        {
            name.resize(name.find('.'));        // source file w/o the 
            break;                              // extension: maybe equal to
        }                                       // the source's function name
    }

    iter = find_if(iter + 1, end,               // maybe find such a name
        [&](size_t idx)
        {                                       // not here...
            if (d_xrefVector[idx].symbol().find(name) == string::npos)
                return false;
            
            d_globals.second = idx;             // but here.
            return true;
        }
    );
}
