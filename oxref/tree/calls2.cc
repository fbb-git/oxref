#include "tree.ih"

// static
bool Tree::calls(size_t symIdx, XrefData const &data)
{
    return      not data.sourceFile().empty() 
            and
                find(data.usedBy().begin(), data.usedBy().end(), symIdx) 
                != data.usedBy().end();
}
