#include "storage.ih"

void Storage::calledBy()
{
    string spec;

    if (d_arg.option(&spec, 'c'))     // no called-by requested
        d_store.calledBy(spec);
}

